package com.urlshortener.exception;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private ApplicationMessageKey messageKey;

	public ApplicationException(ApplicationMessageKey messageKey) {
		super(getMessage(messageKey));
		this.messageKey = messageKey;
	}

	public ApplicationMessageKey getMessageKey() {
		return messageKey;
	}

	private static String getMessage(ApplicationMessageKey messageKey) {
		return messageKey.getMessageDescription();
	}
}
