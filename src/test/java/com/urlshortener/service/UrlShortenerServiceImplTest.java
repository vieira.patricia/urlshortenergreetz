package com.urlshortener.service;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.urlshortener.domain.Url;
import com.urlshortener.dto.UrlShortenerDto;
import com.urlshortener.exception.ApplicationException;
import com.urlshortener.exception.ApplicationMessageKey;
import com.urlshortener.repository.UrlShortenerRepository;
import com.urlshortener.service.UrlShortenerService;
import com.urlshortener.service.UrlShortenerServiceImpl;

public class UrlShortenerServiceImplTest {

	private static String SHORT_URL = "http://gtz.com/1x3cg";
	private static String LONG_URL = "https://www.greetz.nl/cadeaus/detail/kraampakket-neutraal---m/1142798147";
	private static String INVALID_URL = "http/cadeaus/detail/kraampakket-neutraal---m/1142798147g";
	private static Long LONG_URL_ID = 100L;

	UrlShortenerDto urlShortenerDto;

	@Mock
	private UrlShortenerRepository urlShortenerRepository;

	@Mock
	private UrlShortenerService urlShortenerService;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();  

	
	Url url;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		urlShortenerService = new UrlShortenerServiceImpl(urlShortenerRepository);
		url = new Url();
		url.setCreatedOn(new Date());
		url.setId(LONG_URL_ID);
		url.setLongUrl(LONG_URL);
		when(urlShortenerRepository.findByLongUrlId(LONG_URL_ID)).thenReturn(url);
	}

	@Test
	public void getLongUrlTest() {
		urlShortenerDto = urlShortenerService.getLongUrl(SHORT_URL);
		verify(urlShortenerRepository, times(1)).findByLongUrlId(LONG_URL_ID);
		assertThat(urlShortenerDto, is(notNullValue()));
		assertThat(urlShortenerDto, is(instanceOf(UrlShortenerDto.class)));
		assertEquals(urlShortenerDto.getLongUrl(),
				urlShortenerRepository.findByLongUrlId(LONG_URL_ID).getLongUrl());
	}

	@Test
	public void getLongUrlThrowLongUrlNotFoundApplicationException() {
		when(urlShortenerRepository.findByLongUrlId(LONG_URL_ID)).thenReturn(null);
		expectedException.expect(ApplicationException.class);
		expectedException.expectMessage(ApplicationMessageKey.LONG_URL_NOT_FOUND.getMessageDescription());
		urlShortenerService.getLongUrl(SHORT_URL);
	}

	@Test
	public void generateShortUrlTest() {
		when(urlShortenerRepository.findByLongUrl(LONG_URL)).thenReturn(url);
		urlShortenerDto = urlShortenerService.generateShortUrl(LONG_URL);
		assertThat(urlShortenerDto, is(notNullValue()));
		assertEquals(urlShortenerDto.getShortUrl(),
				SHORT_URL);
		verify(urlShortenerRepository, times(1)).findByLongUrl(LONG_URL);
	}

	@Test
	public void generateShortUrlWhenLongUrlDoesNotExistTest() {
		when(urlShortenerRepository.save(any(Url.class))).thenReturn(url);
		when(urlShortenerRepository.findByLongUrl(LONG_URL)).thenReturn(null);
		urlShortenerDto = urlShortenerService.generateShortUrl(LONG_URL);
		verify(urlShortenerRepository, times(1)).save(any(Url.class));
	}

	@Test
	public void generateShortUrlThrowUrlInvalidApplicationException() {
		expectedException.expect(ApplicationException.class);
		expectedException.expectMessage(ApplicationMessageKey.INVALID_URL.getMessageDescription());
		urlShortenerDto = urlShortenerService.generateShortUrl(INVALID_URL);
		when(urlShortenerRepository.findByLongUrl(LONG_URL)).thenReturn(null);
		url.setLongUrl(INVALID_URL);
		when(urlShortenerRepository.save(url)).thenReturn(url);
	}

}
