# Project to create a Url Shortener Java

### Technology Stack:

* Java
* Spring Boot
* Spring JPA
* Spring MVC
* MySQL

URL shortening is a technique on the World Wide Web in which a Uniform Resource Locator (URL) may be made substantially shorter and still direct to the required page. This is achieved by using a redirect which links to the web page that has a long URL. For example, the URL "http://example.com/assets/category_B/subcategory_C/Foo/" can be shortened to "https://example.com/Foo", and the URL "http://example.com/about/index.html" can be shortened to "https://goo.gl/aO3Ssc". Often the redirect domain name is shorter than the original one. 

