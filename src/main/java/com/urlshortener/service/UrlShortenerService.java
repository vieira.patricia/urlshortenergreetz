package com.urlshortener.service;

import com.urlshortener.dto.UrlShortenerDto;

public interface UrlShortenerService {
	/**
	 * Method to retrieve the longUrl from shortUrl
	 * @param shortUrl
	 * @return
	 */
	UrlShortenerDto getLongUrl(String shortUrl);
	
	/**
	 * Methid to generate the short url
	 * @param longUrl
	 * @return 
	 */
	UrlShortenerDto generateShortUrl(String longUrl);
	
}
