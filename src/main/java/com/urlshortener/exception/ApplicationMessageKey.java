package com.urlshortener.exception;

import org.springframework.http.HttpStatus;

/**
 * Enum with general application error
 */
public enum ApplicationMessageKey {

	INVALID_URL("Please enter with a valid url", HttpStatus.BAD_REQUEST),
	LONG_URL_NOT_FOUND("Long url not found", HttpStatus.BAD_REQUEST);

	String messageDescription;
	HttpStatus httpStatus;

	ApplicationMessageKey(String messageDescription, HttpStatus httpCode) {
		this.messageDescription = messageDescription;
		this.httpStatus = httpCode;
	}

	ApplicationMessageKey(String messageDescription) {
		this(messageDescription, HttpStatus.BAD_REQUEST);
	}  

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public String getMessageDescription() {
		return messageDescription;
	}

}
