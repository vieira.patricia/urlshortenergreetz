package com.urlshortener.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.urlshortener.domain.Url;

public interface UrlShortenerRepository extends JpaRepository<Url, Long>{
	
	@Query(value = "from Url u where u.id = :id")
	Url findByLongUrlId(@Param(value = "id") Long id);
	
	@Query(value = "from Url u where u.longUrl = :longUrl")
	Url findByLongUrl(@Param(value = "longUrl") String longUrl);
}
