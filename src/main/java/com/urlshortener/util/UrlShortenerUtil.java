package com.urlshortener.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

import com.urlshortener.domain.Url;

public class UrlShortenerUtil {

	/**
	 * Domain to determine the short url 
	 */
	private static final String URL_DOMAIN = "http://gtz.com";

	/**
	 * Method to create an unique encoded id from a longUrlId
	 * @param longUrlId
	 * @return encoded urlId
	 */
	public static String encodeUrlId(String longUrlId) {
		byte[] bytes = longUrlId.getBytes(StandardCharsets.UTF_8); 
		String urlIdEncoded = new BigInteger(1, bytes).toString(36);

		return urlIdEncoded;
	}

	/**
	 * Method to decode the longUrlId
	 * @param longUrlEncoded
	 * @return decoded urlId
	 */
	public static String decodeUrlId(String longUrlEncoded) {
		byte[] bytes = new BigInteger(longUrlEncoded, 36).toByteArray();
		String urlIdDecoded = new String(bytes, StandardCharsets.UTF_8);

		return urlIdDecoded;
	}

	/**
	 * Method to generate a short url string
	 * @param longUrl
	 * @return short url string
	 */
	public static String generateShortStringUrl(Url longUrl) {
		StringBuilder shortUrl = new StringBuilder();
		Long longUrlId = longUrl.getId();
		String encodedId = encodeUrlId(String.valueOf(longUrlId));
		String shortenedUrl = shortUrl.append(URL_DOMAIN).append("/").append(encodedId).toString();

		return shortenedUrl;
	}

	/**
	 * Method to retrieve the encoded longUrlId from a shortUrl
	 * @param shortUrl
	 * @return url id
	 */
	public static Long getLongUrlIdFromShortUrl(String shortUrl) {
		String shortUrlId =  shortUrl.replace(URL_DOMAIN + "/", "");
		String decodedLongUrlId = decodeUrlId(shortUrlId);
		Long encodedShortUrl = Long.parseLong(decodedLongUrlId);

		return encodedShortUrl;
	}
}
