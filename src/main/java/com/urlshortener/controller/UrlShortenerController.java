package com.urlshortener.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.urlshortener.dto.UrlShortenerDto;
import com.urlshortener.service.UrlShortenerService;

@Controller
@RequestMapping(path="/urlshortener") 
public class UrlShortenerController {
	private UrlShortenerService urlShortenerService;

	public UrlShortenerController(UrlShortenerService urlShortenerService) {
		this.urlShortenerService = urlShortenerService;
	}

	/**
	 * Method to generate the short url
	 * @param longUrl
	 * @return UrlShortenerDto
	 */
	@RequestMapping(value = "/shortUrl",
			method = RequestMethod.POST,
			produces = {"application/json"})
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public UrlShortenerDto generateShortUrl(@RequestParam(value = "longUrl") String longUrl) {
		return urlShortenerService.generateShortUrl(longUrl);
	}

	/**
	 * Method to retrieve the longUrl from shortUrl
	 * @param shortUrl
	 * @return
	 */
	@RequestMapping(value = "/longUrl", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public UrlShortenerDto getLongUrl(@RequestParam(value = "shortUrl") String shortUrl) {
		return urlShortenerService.getLongUrl(shortUrl);
	}

}
