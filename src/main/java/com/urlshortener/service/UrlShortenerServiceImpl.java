package com.urlshortener.service;

import java.util.Date;

import org.apache.commons.validator.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import com.urlshortener.domain.Url;
import com.urlshortener.dto.UrlShortenerDto;
import com.urlshortener.exception.ApplicationException;
import com.urlshortener.exception.ApplicationMessageKey;
import com.urlshortener.repository.UrlShortenerRepository;
import com.urlshortener.util.UrlShortenerUtil;

@Service
public class UrlShortenerServiceImpl implements UrlShortenerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UrlShortenerServiceImpl.class);
	private UrlShortenerRepository urlShortenerRepository;

	public UrlShortenerServiceImpl(UrlShortenerRepository urlShortenerRepository) {
		this.urlShortenerRepository = urlShortenerRepository;
	}

	@Override
	public UrlShortenerDto getLongUrl(String shortUrl) {
		LOGGER.info("Received the shortUrl: " + shortUrl);
		UrlShortenerDto urlShortenerDto = new UrlShortenerDto();

		Url url = getLongUrlFromShortUrl(shortUrl);

		urlShortenerDto.setLongUrl(url.getLongUrl());
		urlShortenerDto.setShortUrl(shortUrl);

		return urlShortenerDto;
	}

	@Override
	public UrlShortenerDto generateShortUrl(@NonNull String longUrl) {
		UrlShortenerDto urlShortenerDto = new UrlShortenerDto();
		LOGGER.info("Received the longUrl: " + longUrl);
		Url longUrlPersisted = urlShortenerRepository.findByLongUrl(longUrl);	

		if (longUrlPersisted == null) {
			longUrlPersisted = saveLongUrl(longUrl);
		}
		
		urlShortenerDto.setLongUrl(longUrlPersisted.getLongUrl());
		urlShortenerDto.setShortUrl(generateShortUrlFromLongUrl(longUrlPersisted).getShortUrl());
		LOGGER.info("Generated short url: " + urlShortenerDto.getShortUrl());

		return urlShortenerDto;
	}

	/**
	 * Method to save the long url into the db
	 * @param longUrl
	 * @return a persisted Url
	 */
	
	private Url saveLongUrl(@NonNull String longUrl) {
		Url urlPersisted = null;
		UrlValidator urlValidator = new UrlValidator();

		if (!urlValidator.isValid(longUrl)) {
			throw new ApplicationException(ApplicationMessageKey.INVALID_URL);
		}

		LOGGER.info("Persisting longUrl: " + longUrl);
		urlPersisted = new Url(longUrl, new Date());
		urlPersisted = urlShortenerRepository.save(urlPersisted);

		return urlPersisted;
	}

	/**
	 * Method to generate shortUrl from longUrl
	 * @param url
	 * @return urlShortenerDto 
	 */
	private UrlShortenerDto generateShortUrlFromLongUrl(Url url) {
		UrlShortenerDto urlShortenerDto = new UrlShortenerDto();
		urlShortenerDto.setLongUrl(url.getLongUrl());
		String shortUrl = UrlShortenerUtil.generateShortStringUrl(url);
		urlShortenerDto.setShortUrl(shortUrl);

		return urlShortenerDto;
	}

	/**
	 * Method to retrieve longUrl from a shortUrl
	 * @param shortUrl
	 * @return persisted url 
	 */
	private Url getLongUrlFromShortUrl(String shortUrl) {
		LOGGER.info("Retrieving longUrl by shortUrl: " + shortUrl);
		Long longUrlId = UrlShortenerUtil.getLongUrlIdFromShortUrl(shortUrl);
		Url url = urlShortenerRepository.findByLongUrlId(longUrlId);	

		if (url == null) {
			LOGGER.warn("Long url not found: Id - " + longUrlId);
			throw new ApplicationException(ApplicationMessageKey.LONG_URL_NOT_FOUND);
		}

		return url;
	}
}
